# README #

This README would normally document whatever steps are necessary to get your application up and running.

PS. If you refresh the page you would be automatically log out.

PS2. After doing any things (such as add product to our shop or delete product from the shop), you have to re-open the page

### What is this repository for? ###

this is the repository of OPL project name "Shopping-mania" front-end (react)

### How do I get set up? ###

to run is

```npm install```

```npm start```

### Contribution guidelines ###

before running the code:

change the ip address in 4 files:

* ./AxiosConfiguration.js
* ./CartCard.js
* ./ProductCard.js
* ./SellerProductCard.js

to the ip address of the computer running back-end

### Who do I talk to? ###

* Jarosporn Hansuekkaew (works on front-end)
* Pongwasin Wintakorn (work on back-end)