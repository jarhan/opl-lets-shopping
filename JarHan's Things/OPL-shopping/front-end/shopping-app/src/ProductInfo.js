import React from 'react'
import axios from 'axios'

import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'

export default class ProductInfo extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div class="wrapper">

          <div class="product group">
            <div class="col-1-2 product-image">
              <div class="bg"></div>
              <div class="indicator">
                <div class="dot one"></div>
                <div class="dot two"></div>
                <div class="dot three"></div>
                <div class="dot four"></div>
                <div class="dot five"></div>
              </div>
            </div>
            <div class="col-1-2 product-info">
              <h1>Field Notes Cherry Graph 3-Pack</h1>
              <h2>$7.50</h2>

              <div class="select-dropdown">
                <select>
                  <option value="size">Size</option>
                  <option value="size">Small</option>
                  <option value="size">Medium</option>
                  <option value="size">Large</option>
                </select>
              </div>

              <div class="select-dropdown">
                <select>
                  <option value="quantity">1</option>
                  <option value="quantity">2</option>
                  <option value="quantity">3</option>
                  <option value="quantity">4</option>
                </select>
              </div>

              <br/>

              <a href="" class="add-btn">Add To Cart</a>

              <p>I am using <a href="https://dribbble.com/shots/2063472-Single-Product-Template?list=users&offset=2">this Dribbble shot by Olly Sorsby</a> to practice front-end stuff. Eget lacinia odio sem nec elit. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam id dolor id nibh ultricies.</p>

              <ul>
                <li>Graph paper 40-page memo book.</li>
                <li>3 book per pack. Banded and shrink-wrapped</li>
                <li>Three great memo books worth fillin' up with information</li>
                <li>Red cherry wood covers</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
