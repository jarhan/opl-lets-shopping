import React from 'react'
import axios from 'axios'
import ProductInfo from './ProductInfo'
import SignIn from './SignIn'
import AddProduct from './AddProduct'
import ProductCard from './ProductCard'
import {onAddItem} from './App'

import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'

import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import SvgIcon from 'material-ui/SvgIcon';

export default class Product extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: 1,
    };
  }

  handleChange = (event, index, value) => this.setState({value});

  render() {
    const {value, productsCollector} = this.state
    return (
      <div>
        <div className="pen-title">
          <h2>
            All Products
          </h2>
        </div>
        <div style={{ 'text-align': 'justify', 'display': '-ms-flexbox', 'display': 'flex', '-ms-flex-wrap': 'wrap', 'flex-wrap': 'wrap'}}>
            {this.props.allProductsTest.map((each) => {
              return <ProductCard
                        pid={each._1}
                        photoSrc={each._1}
                        photoAlt={each._1}
                        title={each._4}
                        description={each._5}
                        price={each._2}
                        onAddItem={this.props.onAddItem}/>
            })}
        </div>
      </div>
    )
  }
}
