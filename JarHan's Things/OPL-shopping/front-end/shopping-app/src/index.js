import SignIn from './SignIn'
import Register from './SignUp'
import Products from './Product'
import Seller from './Seller'
import ProductInfo from './ProductInfo'
import AddProduct from './AddProduct'
import CartCard from './CartCard'

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const muiTheme = getMuiTheme({
  palette: {
    textColor: '#919191',
  },
  appBar: {
    height: 50,
  },
});

function Main(){
  return (
    <MuiThemeProvider muiTheme={muiTheme}>
      <Router>
        <App>
          <Route path="/info" component={ProductInfo}/>
          <Route path="/add-product" component={AddProduct}/>
          <Route path="/seller" component={Seller}/>
          <Route exact path="/" component={Products}/>
          <Route path="/register" render={()=>{
            return <Register onLoginSuccess={this.onLoginSuccess.bind(this)}/>
          }}/>
          <Route path="/login" render={()=>{
            return <SignIn onLoginSuccess={this.onLoginSuccess.bind(this)}/>
          }}/>
        </App>
      </Router>
    </MuiThemeProvider>
  )
}

ReactDOM.render(<Main/>, document.getElementById('root'));
registerServiceWorker();
