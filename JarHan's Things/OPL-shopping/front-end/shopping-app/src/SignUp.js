import React from 'react'
import axios from './AxiosConfiguration'
import FlatButton from 'material-ui/FlatButton'

function SignUpView({
  username,
  password,
  errorMessage,
  onChange,
  onSignUp
}) {
  return (
    <div>
      <div class="card-container">
        <div class="card"></div>
        <div class="card">
          <h1 class="title">Register</h1>
          <form>
                <div class="input-container">
                  <input
                    type="text"
                    id="#{label}"
                    required="required"
                    value={username}
                    onChange = {(e)=>onChange(e.target.value, password)}
                  />
                  <label for="#{label}">Username</label>
                  <div class="bar"></div>
                </div>
                <div class="input-container">
                  <input
                    type="password"
                    id="#{label}"
                    required="required"
                    value={password}
                    onChange = {(e)=>onChange(username, e.target.value)}
                  />
                  <label for="#{label}">Password</label>
                  <div class="bar"></div>
                </div>
                <div>
                  <div className="pen-title">
                    <span>
                      Already has an account?
                    </span>
                    <a href="/login"><FlatButton label="Login" secondary={true} /></a>
                  </div>
                </div>
                <div
                  class="button-container"
                  onClick={() => onSignUp(username, password)}
                  >
                    <span>Next</span>
                </div>
              </form>
        </div>
      </div>
    </div>
  )
}


export default class Login extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      username: '',
      password: '',
      errorMessage: ''
    }
  }

  onChange(username, password) {
    this.setState({username, password})
  }

  onSignUp(username, password) {
    console.log('attempt to SignIn', {username, password})
    axios
      .post('/register', {username, password})
      .then(res => {
        console.log("YEAHHHHHH", res.data.username)
        // this.props.onSignUpSuccess(res.data.username)
      }).catch(err => {
        this.setState({errorMessage: 'Something went wrong'})
      })
  }

  render() {
    const {username, password, errorMessage} = this.state
    return (<SignUpView
      username = {username}
      password = {password}
      errorMessage = {errorMessage}
      onChange = {this.onChange.bind(this)}
      onSignUp = {this.onSignUp.bind(this)}
    />)
  }
}
