import React from 'react';
import './App.css'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'

import Clear from 'material-ui/svg-icons/content/clear';
import {white} from 'material-ui/styles/colors';

export default function ProductCard({pid, title, photoSrc, photoAlt, description, price, onDeleteItem}) {
  return (
    <figure class="snip1423"><img src={"http://10.99.243.90:8080/products/"+pid} alt={photoAlt} />
      <figcaption>
        <h3>{title}</h3>
        <p>{description}</p>
        <div class="price">
          ${price}
        </div>
      </figcaption>

      <i class="material-icons" onClick={() => onDeleteItem(photoSrc, photoAlt, title, price, description)}><Clear color={white}/></i>

    </figure>
  )
}
