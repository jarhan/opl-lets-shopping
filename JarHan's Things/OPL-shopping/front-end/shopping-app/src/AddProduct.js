import React from 'react'
import { withRouter } from 'react-router-dom'
import axios from './AxiosConfiguration'
import Seller from './Seller'

import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import UploadFile from 'material-ui/svg-icons/file/file-upload';
import TextField from 'material-ui/TextField';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'

const styles = {
  uploadInput: {
    cursor: 'pointer',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
  },
};

class AddProduct extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: '',
      title: 'OREO', // '',
      price: '12',
      description: 'Aroi Mak',
      image: ''
    };
    this._handleImageChange = this._handleImageChange.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
  }

  _handleSubmit(e) {
    e.preventDefault();
    // TODO: do something with -> this.state.file
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file)
  }
  onAddProduct(title, price, description, image) {
    const formData = new FormData();
    formData.append('title', title)
    formData.append('price', price)
    formData.append('description', description)
    formData.append('image', image)
    formData.append('username', this.props.username)
    const config = {
          headers: {
              'content-type': 'multipart/form-data'
          }
      }
    axios
      .post('/products/add', {
        title: title,
        price: price,
        description: description,
        username: this.props.username,
        image: "data=" + encodeURIComponent(image)
      })
      .then(res => {
        this.props.history.push('/{username}')
      }).catch(err => {
        this.setState({errorMessage: 'Something went wrong'})
      })
  }

  render() {
    const {title, price, description, image} = this.state

    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img src={imagePreviewUrl} style={{'max-width': '-webkit-fill-available', 'max-height': '-webkit-fill-available'}}/>);
    }

    return (
      <div class="container" id="app">
          <form>
            <div class="form--field">
              <label>Product Title *</label>
              <input
                type="text"
                class="form--element"
                name="title"
                v-model="productData.title"
                placeholder="Title"
                required=""
                value={title}
                onChange = {(e) => this.setState({title: e.target.value})}
              />
            </div>
            <div class="form--container -inline">
              <div class="form--field -short">
                <label>Product Price *</label>
                <span class="form--price">$</span>
                <input
                  type="text"
                  class="form--element"
                  name="price"
                  v-model="productData.price"
                  placeholder="Price"
                  required=""
                  min="0"
                  max="500"
                  pattern="\d+(\.\d{2})?"
                  value={price}
                  onChange = {(e) => this.setState({price: e.target.value})}
                />
              </div>
            </div>
            <div class="form--field">
              <label>Product Description</label>
              <textarea
                class="form--element textarea"
                v-model="productData.description"
                placeholder="Description"
                value={description}
                onChange = {(e) => this.setState({description: e.target.value})}
              >
              </textarea>
            </div>
            <div class="form--field">
              <label>Product Image *</label>
              <FlatButton
                label="Choose an Image"
                labelPosition="before"
                style={styles.uploadButton}
                containerElement="label"
              >
                <input
                  type="file"
                  style={styles.uploadInput}
                  onChange={this._handleImageChange}
                />
              </FlatButton>
              {$imagePreview}

            </div>
            <Link to="/Seller" style={{'text-decoration': 'none'}}><button type="submit" class="submit-button" onClick={() => this.onAddProduct(title, price, description, imagePreviewUrl)}>Add Product</button></Link>
          </form>
          <Route path="/Seller" component={Seller}/>
      </div>
    )
  }
}

export default withRouter(AddProduct)
