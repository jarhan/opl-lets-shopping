import React from 'react';
import './App.css'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'

export default function CartCard({title, photoSrc, photoAlt, price, quantity, onRemoveItem}) {
  return (
    <li class="clearfix">
      <img src={"http://192.168.2.114:8080/products/"+photoSrc} alt={photoAlt} height="70" width="70"/>
      <span class="item-name">{title}</span>
      <span class="item-price">${price}</span>
      <span class="item-quantity">Quantity: {quantity}</span>
      <h2 onClick={() => onRemoveItem(photoSrc, photoAlt, title, price)}>remove</h2>
    </li>
  )
}
