import React from 'react';
import './App.css'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'
import Axios from './AxiosConfiguration'

export default function ProductCard({pid, title, photoSrc, photoAlt, description, price, onAddItem}) {
  return (
    <figure class="snip1423"><img src={"http://192.168.2.114:8080/products/"+pid} alt={photoAlt} />
      <figcaption>
        <h3>{title}</h3>
        <p>{description}</p>
        <div class="price">
          ${price}
        </div>

      </figcaption>

      <i class="fa fa-shopping-cart cart-icon" onClick={() => onAddItem(photoSrc, photoAlt, title, price)}></i>

    </figure>
  )
}
