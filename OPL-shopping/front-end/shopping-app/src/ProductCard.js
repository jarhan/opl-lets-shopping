import React from 'react';
import './App.css'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'

export default function ProductCard({pid, title, photoSrc, photoAlt, description, price, onAddItem}) {
  // console.log(pid);
  return (
    <figure class="snip1423"><img src={"http://10.99.243.90:8080/products/"+pid} alt={photoAlt} />
      <figcaption>
        <h3>{title}</h3>
        <p>{description}</p>
        <div class="price">
          ${price}
        </div>

        {/* Edit to get info of each product or DELETE info page off ^_^ */}

        {/* <Link to="/info">
          <p2>
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="15" viewBox="0 0 18 12"><path d="M12.44 6.44L9 9.88 5.56 6.44 4.5 7.5 9 12l4.5-4.5z"/></svg>
            More
          </p2>
        </Link> */}

      </figcaption>

      {/* Edit to add to cart */}

      <i class="fa fa-shopping-cart cart-icon" onClick={() => onAddItem(photoSrc, photoAlt, title, price)}></i>

    </figure>
  )
}
