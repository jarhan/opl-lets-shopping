import Axios from "axios";


export default Axios.create({
  baseURL: 'http://10.99.243.90:8080',
  withCredentials: true,
  headers: {
    common: {
      Accept: 'application/json'
    },
  }
})
