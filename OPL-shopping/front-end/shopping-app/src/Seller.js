import React from 'react'
import axios from './AxiosConfiguration'
// import ProductInfo from './ProductInfo'
import SignIn from './SignIn'
import AddProduct from './AddProduct'
import SellerProductCard from './SellerProductCard'

import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'

import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import SvgIcon from 'material-ui/SvgIcon';
import AddIcon from 'material-ui/svg-icons/content/add';
import {white} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';

export default class Product extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: 1,
      // productsCollector: []
    };
  }

  handleChange = (event, index, value) => this.setState({value});

  onDeleteItem(photoSrc, photoAlt, title, price, description) {
    // const collector = this.state.productsCollector
    // const newCollector = collector.filter(function(item) {
    //   return (item.photoSrc !== photoSrc) ||
    //          (item.photoAlt !== photoAlt) ||
    //          (item.title !== title) ||
    //          (item.price !== price) ||
    //          (item.description !== description);
    // });
    // // console.log(toDelete);
    // console.log(newCollector);
    // this.setState({productsCollector: newCollector})
    axios
    .get("/products/remove/"+photoSrc)
    .then(res => {
      console.log(res);
      this.props.history.push('/Seller')
    })
    .catch(err => {
      console.log("OMGGGGGGGG in remove bru")
      console.log(err)
    })
  }

  render() {
    const {value} = this.state
    console.log("in seller");
    console.log(this.props.userProducts);
    console.log("end seller");
    return (
      <div>
        <div className="pen-title">
          <h2>
            Your Products
          </h2>
        </div>
        <div style={{ 'text-align': 'justify', 'display': '-ms-flexbox', 'display': 'flex', '-ms-flex-wrap': 'wrap', 'flex-wrap': 'wrap'}}>
            {this.props.userProducts.map((each) => {
              return <SellerProductCard
                        pid={each._1}
                        photoSrc={each._1}
                        photoAlt={each._1}
                        title={each._4}
                        description={each._5}
                        price={each._2}
                        onDeleteItem={this.onDeleteItem.bind(this)}/>
            })}

            <figure class="add-product-icon">
              <Link to="/add-product">
                <i class="material-icons"><AddIcon color={white}/></i>
              </Link>
            </figure>
        </div>
        <Route path="/add-product" component={AddProduct}/>
      </div>
    )
  }
}
