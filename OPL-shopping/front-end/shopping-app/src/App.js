import React, { Component} from 'react';
// import Login from './Login'
import SignIn from './SignIn'
import Register from './SignUp'
import Products from './Product'
import Seller from './Seller'
import ProductInfo from './ProductInfo'
import AddProduct from './AddProduct'
import CartCard from './CartCard'

import {grey600} from 'material-ui/styles/colors';
import injectTapEventPlugin from 'react-tap-event-plugin';
import logo from './logo.svg';
import {Tabs, Tab} from 'material-ui/Tabs';
import FontIcon from 'material-ui/FontIcon';
import MapsPersonPin from 'material-ui/svg-icons/maps/person-pin';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/create';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';

import Popover, {PopoverAnimationVertical} from 'material-ui/Popover';

import RaisedButton from 'material-ui/RaisedButton';
// import Menu from 'material-ui/Menu';
import SvgIcon from 'material-ui/SvgIcon';
import MenuItem from 'material-ui/MenuItem';

import Dialog from 'material-ui/Dialog';
import DatePicker from 'material-ui/DatePicker';

import axios from './AxiosConfiguration'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'
import { withRouter } from 'react-router'

import Badge from 'material-ui/Badge';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';

import Drawer from 'material-ui/Drawer';

import './App.css';

injectTapEventPlugin();

const iconStyles = {
  marginRight: 24,
  top: 10,
};

const HomeIcon = (props) => (
  <SvgIcon {...props}>
    <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
  </SvgIcon>
);

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
};

const style = {
  marginRight: 30,
};

const tab_style = {
  title: {
    cursor: 'pointer',
  },
};

const actions = [
  <FlatButton
    label="Ok"
    primary={true}
    keyboardFocused={true}
    onClick={this.handleClose}
  />,
];

// function Register() {
//   return (<div> Register Page</div>)
// }

function Logout() {
  return (<div> Logout Page</div>)
}

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      // value: 'a',
      open: false,
      openLogin: false,
      openCheckout: false,
      username: null,
      openDrawer: false,
      totalPrice: 0,
      countProduct: 0,
      quantity: 1,
      cartMsg: "",
      cartCollector: [],
      // productsCollector: [
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample57.jpg",
      //     photoAlt: "sample57",
      //     title: "Pudol Doux",
      //     description: "All this modern technology just makes people try to do everything at once.",
      //     price: "19.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample98.jpg",
      //     photoAlt: "sample98",
      //     title: "Kurie Secco",
      //     description: "To make a bad day worse, spend it wishing for the impossible.",
      //     price: "39.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample90.jpg",
      //     photoAlt: "sample90",
      //     title: "Zosaisan Invec",
      //     description: "Why should I have to work for everything? It's like saying that I don't ",
      //     price: "45.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample57.jpg",
      //     photoAlt: "sample57",
      //     title: "Pudol Doux",
      //     description: "All this modern technology just makes people try to do everything at once.",
      //     price: "19.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample98.jpg",
      //     photoAlt: "sample98",
      //     title: "Kurie Secco",
      //     description: "To make a bad day worse, spend it wishing for the impossible.",
      //     price: "39.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample90.jpg",
      //     photoAlt: "sample90",
      //     title: "Zosaisan Invec",
      //     description: "Why should I have to work for everything? It's like saying that I don't ",
      //     price: "45.00"
      //   }
      // ],
      userProducts: [],
      // [{
      //   photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample90.jpg",
      //   photoAlt: "sample90",
      //   title: "Zosaisan Invec",
      //   description: "Why should I have to work for everything? It's like saying that I don't ",
      //   price: "45.00"
      // }],
      userProductsTest: [],
      allProductsTest: []
    };
  }

  componentDidMount() {
    console.log("COMPONENT DID MOUNT")
    axios.get('/all').then(res => {
      console.log("in componentDidMount");
      console.log(res.data)
      this.setState({allProductsTest: res.data})
      console.log(this.state.allProductsTest[0]._1);
    })
  }

  // cart
  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();
    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = (event) => {
    this.setState({open: false,});
  };

  // log in
  handleOpen = () => {
    this.setState({openLogin: true});
  };

  handleClose = () => {
    this.setState({openLogin: false});
  };

  onLoginSuccess(username) {
    this.setState({username})
    // this.props.history.push('/app/list-hand')
  }

  onLogout() {
    axios.get('/logout').then(() =>{
      this.setState({username: null})
      this.props.history.push('/app/login')
    })
  }

  // drawer
  handleToggleDrawer = (event) => {
    this.setState({openDrawer: !this.state.openDrawer});
  }

  handleCloseDrawer = (event) => {
    this.setState({openDrawer: false});
  }

  handleOpenCheckout = () => {
    this.setState({openCheckout: true,
                   open: !this.state.open,
                   cartCollector: [],
                   cartMsg: "Your Cart is Empty."})
  };

  handleCloseCheckout = () => {
    console.log("in handleCloseCheckout");
    this.setState({openCheckout: false});
  };

  onCheckout = () => {
    this.setState({open: !this.state.open, cartCollector: []})
  }

  onAddItem(photoSrc, photoAlt, title, price) {
    console.log("onAddItem");
    const collector = this.state.cartCollector
    const toAdd = collector.filter(function(item) {
      return (item.photoSrc == photoSrc) &&
             (item.photoAlt == photoAlt) &&
             (item.title == title) &&
             (item.price == price);
    });

    const leftProducts = collector.filter(function(item) {
      return (item.photoSrc !== photoSrc) ||
             (item.photoAlt !== photoAlt) ||
             (item.title !== title) ||
             (item.price !== price);
    });
    console.log("leftProducts");
    console.log(leftProducts);

    let quantity = 1

    console.log("toAdd");
    console.log(toAdd);
    if (toAdd.length == 0) {
      console.log("if");
      console.log(this.state.quantity);
    }
    else {
      console.log("else");
      quantity = toAdd[0].quantity + 1
      console.log(toAdd[0].quantity);
      console.log(this.state.quantity);
    }

    const newProduct = {photoSrc: photoSrc, photoAlt: photoAlt, title:title, price: price, quantity: quantity}
    const newCartCollector = [newProduct, ...leftProducts]
    console.log(newProduct);
    this.setState({cartCollector: newCartCollector, cartMsg: ""})
  }

  onGetUserProducts(username) {
    console.log("in onget");
    console.log(username);
    console.log('user/products/'+username);
    axios.post('user/products/'+username)
    .then(res => {
      console.log("YEAHHHHHH", res)
      this.setState({userProducts: res.data})
      console.log("userProducts");
      console.log(this.state.userProducts);
      // console.log(this.state.userProductsTest.data[0]);
      // this.props.onSignUpSuccess(res.data.username)
    }).catch(err => {
      // this.setState({errorMessage: 'Something went wrong'})
      console.log("err in on get");
    })
  }

  render() {
    const {username, cartCollector, cartMsg, userProducts} = this.state
    console.log("in app", username);
    const actionsCheckout = [
      <FlatButton
        label="OK"
        primary={true}
        onClick={this.handleCloseCheckout}
      />,
    ];

    let sum = a => a.reduce((n, x) => n + x, 0);
    let totalPrice = sum(cartCollector.map(x => Number(x.price) * Number(x.quantity)));
    let countProduct = sum(cartCollector.map(x => 1))

    return (
      <div>

        <div className="pen-title">
          <h1>Shopping-Mania</h1>
          <span>
            Back to the <Link to="/" style={{'text-decoration': 'none'}}>home</Link>
          </span>
        </div>
        <div style={{padding: '0% 10% 0%'}}>
        <nav>
          <div class="container-st">
            <ul class="navbar-left">
              <div>
                  <IconButton tooltip="Menu" onClick={this.handleToggleDrawer}>
                    <MenuIcon color={grey600}/>
                  </IconButton>
                <Drawer
                  docked={false}
                  width={200}
                  open={this.state.openDrawer}
                  onRequestChange={(openDrawer) => this.setState({openDrawer})}
                >
                  <Link to="/" style={{'text-decoration': 'none'}}><MenuItem onClick={this.handleCloseDrawer}><li><HomeIcon style={iconStyles} color={grey600}/></li></MenuItem></Link>
                  <Link to="/register" style={{'text-decoration': 'none'}}><MenuItem onClick={this.handleCloseDrawer}><li>Register</li></MenuItem></Link>

                  {username === null &&
                  <Link to="/login" style={{'text-decoration': 'none'}}><MenuItem onClick={this.handleCloseDrawer}><li>Login</li></MenuItem></Link>}
                  {!(username === null) &&
                  <Link to="/Seller" style={{'text-decoration': 'none'}}><MenuItem onClick={(event) => {
                    console.log(username);
                    this.handleCloseDrawer();
                    this.onGetUserProducts(username);}}><li>Your Shop</li></MenuItem></Link>}

                  <MenuItem onClick={this.handleCloseDrawer}><li><a onClick={this.onLogout.bind(this)}>Logout</a></li></MenuItem>

                </Drawer>
              </div>
            </ul>

            <ul class="navbar-right">
              <li onClick={this.handleTouchTap}>
                <span>
                  <Badge
                    badgeContent={countProduct}
                    secondary={true}
                    badgeStyle={{top: 28, "font-size": 10, right: 16, width: 16, height: 16}}
                  >
                    <IconButton tooltip="Cart">
                      <i class="fa fa-shopping-cart cart-icon"></i>
                    </IconButton>
                  </Badge>
                </span>
                <Popover
                  open={this.state.open}
                  anchorEl={this.state.anchorEl}
                  anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                  targetOrigin={{horizontal: 'left', vertical: 'top'}}
                  onRequestClose={this.handleRequestClose}
                  animation={PopoverAnimationVertical}
                  style="transition: transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
                          opacity: 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
                          box-sizing: border-box;font-family: Roboto, sans-serif;
                          -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
                          position: fixed;
                          z-index: 2100;
                          opacity: 1;
                          transform: scaleY(1);
                          transform-origin: left top 0px;
                          max-height: 826px;
                          top: 238px;
                          left: 652px;
                        "
                >
                  <div class="shopping-cart">
                    <div class="shopping-cart-header">
                      <i class="fa fa-shopping-cart cart-icon"></i>
                      <span class="badge">
                        {countProduct}
                      </span>
                      <div class="shopping-cart-total">
                        <span class="lighter-text">Total:</span>
                        <span class="main-color-text">
                          ${totalPrice.toFixed(2)}
                        </span>
                      </div>
                    </div>
                    <h2 style={{"text-align": "center"}}>{cartMsg}</h2>

                    <ul class="shopping-cart-items">
                      {cartCollector.map((each) => {
                        return <CartCard photoSrc={each.photoSrc} photoAlt={each.photoAlt} title={each.title} price={each.price} quantity={each.quantity}/>
                      })}
                    </ul>

                    <a class="button" onClick={this.handleOpenCheckout}>
                      Checkout
                    </a>

                    </div>
                </Popover>
                <Dialog
                  title="Checkout"
                  actions={actionsCheckout}
                  modal={false}
                  open={this.state.openCheckout}
                  onRequestClose={this.handleCloseCheckout}
                >
                  We have sent your order(s) to the seller. Please mail your payment receipt to the seller.
                </Dialog>
              </li>
            </ul>
          </div>
        </nav>
        <div/>
        {/* <Route path="/info" component={ProductInfo}/> */}
        {/* <Route path="/add-product" component={AddProduct}/> */}
        <Route path="/add-product" render={()=>{
          return <AddProduct username={username}/>
        }}/>
        {/* <Route path="/Seller" component={Seller}/> */}
        <Route path="/Seller" render={()=>{
          return <Seller userProducts={this.state.userProducts}/>
        }}/>
        {/* <Route exact path="/" component={Products}/> */}
        <Route exact path="/" render={()=>{
          return <Products onAddItem={this.onAddItem.bind(this)} allProductsTest={this.state.allProductsTest}/>
        }}/>
        <Route path="/register" component={Register}/>
        <Route path="/login" render={()=>{
          return <SignIn onLoginSuccess={this.onLoginSuccess.bind(this)}/>
        }}/>
      </div>
    </div>
    );
  }
}

export default withRouter(App);
