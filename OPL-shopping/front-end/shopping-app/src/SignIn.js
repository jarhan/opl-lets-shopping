import React from 'react'
import axios from './AxiosConfiguration'
import FlatButton from 'material-ui/FlatButton'
import { Redirect } from 'react-router';
import { withRouter } from 'react-router-dom'

function LoginView({username, password, errorMessage, onChange, onLogin}) {
  return (
    <div>
      <div className="card-container">
        <div className="card"></div>
        <div className="card">
          <h1 className="title">Login</h1>
          <form>
            <div className="input-container">
              <input type="text"
                     id="#{label}"
                     required="required"
                     value={username}
                     onChange={(e)=>onChange(e.target.value, password)}
              />
              <label for="#{label}">Username</label>
              <div className="bar"></div>
            </div>
            <div className="input-container">
              <input type="password"
                     id="#{label}"
                     required="required"
                     value={password}
                     onChange = {(e)=>onChange(username, e.target.value)}
              />
              <label for="#{label}">Password</label>
              <div class="bar"></div>
            </div>

            {/* <p>Error: {errorMessage}</p> */}

            <div>
              <div className="pen-title">
                <span>
                  Does not have an account yet?
                </span>
                <a href="/register"><FlatButton label="Register" secondary={true} /></a>
              </div>
            </div>
            <div
              class="button-container"
              onClick={() => onLogin(username, password)}
            >
              {/* <button> */}
                <span>Go</span>
              {/* </button> */}
            </div>
            <div class="footer"><a href="#">Forgot your password?</a></div>
          </form>
        </div>
      </div>
    </div>
  )
}


class SignIn extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      username: 'Pamtest18',
      password: '123456789',
      errorMessage: ''
    }
  }

  onChange(username, password) {
    this.setState({username, password})
  }

  onLogin(username, password) {
    console.log('attempt to Login', {username, password})
    axios
      .post('/login', {username, password})
      .then(res => {
        console.log("YEAHHHHHH", res)
        this.props.onLoginSuccess(username)
        this.props.history.push('/')
      }).catch(err => {
        console.log("OMGGGGGGGG")
        console.log(err)
        this.setState({errorMessage: 'Something went wrong'})
      })
  }

  render() {
    const {username, password, errorMessage} = this.state

    return (
      <LoginView
        username = {username}
        password = {password}
        errorMessage = {errorMessage}
        onChange = {this.onChange.bind(this)}
        onLogin = {this.onLogin.bind(this)}
      />
      // {!(username === null) &&
      //   (<Redirect to="/seller"/>)
      // }
    )
  }
}

export default withRouter(SignIn)
