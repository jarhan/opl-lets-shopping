import React from 'react'
import axios from 'axios'
import ProductInfo from './ProductInfo'
import SignIn from './SignIn'
import AddProduct from './AddProduct'
import ProductCard from './ProductCard'
import {onAddItem} from './App'

import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom'

import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import SvgIcon from 'material-ui/SvgIcon';

export default class Product extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: 1,
      // productsCollector:
      // [
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample57.jpg",
      //     photoAlt: "sample57",
      //     title: "Pudol Doux",
      //     description: "All this modern technology just makes people try to do everything at once.",
      //     price: "19.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample98.jpg",
      //     photoAlt: "sample98",
      //     title: "Kurie Secco",
      //     description: "To make a bad day worse, spend it wishing for the impossible.",
      //     price: "39.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample90.jpg",
      //     photoAlt: "sample90",
      //     title: "Zosaisan Invec",
      //     description: "Why should I have to work for everything? It's like saying that I don't ",
      //     price: "45.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample57.jpg",
      //     photoAlt: "sample57",
      //     title: "Pudol Doux",
      //     description: "All this modern technology just makes people try to do everything at once.",
      //     price: "19.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample98.jpg",
      //     photoAlt: "sample98",
      //     title: "Kurie Secco",
      //     description: "To make a bad day worse, spend it wishing for the impossible.",
      //     price: "39.00"
      //   },
      //   {
      //     photoSrc: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample90.jpg",
      //     photoAlt: "sample90",
      //     title: "Zosaisan Invec",
      //     description: "Why should I have to work for everything? It's like saying that I don't ",
      //     price: "45.00"
      //   }
      // ]
    };
  }

  handleChange = (event, index, value) => this.setState({value});

  render() {
    const {value, productsCollector} = this.state
    return (
      <div>
        <div className="pen-title">
          <h2>
            All Products
          </h2>
        </div>
        {/* <div style={{ 'text-align': 'justify', 'display': '-ms-flexbox', 'display': 'flex', '-ms-flex-wrap': 'wrap', 'flex-wrap': 'wrap'}}>
            {productsCollector.map((each) => {
              return <ProductCard
                        photoSrc={each.photoSrc}
                        photoAlt={each.photoAlt}
                        title={each.title}
                        description={each.description}
                        price={each.price}
                        onAddItem={this.props.onAddItem}/>
            })}
        </div> */}

        <div style={{ 'text-align': 'justify', 'display': '-ms-flexbox', 'display': 'flex', '-ms-flex-wrap': 'wrap', 'flex-wrap': 'wrap'}}>
            {this.props.allProductsTest.map((each) => {
              return <ProductCard
                        pid={each._1}
                        photoSrc={each._1}
                        photoAlt={each._1}
                        title={each._4}
                        description={each._5}
                        price={each._2}
                        onAddItem={this.props.onAddItem}/>
            })}
        </div>
      </div>
    )
  }
}
